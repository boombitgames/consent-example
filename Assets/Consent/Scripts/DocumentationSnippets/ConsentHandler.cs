﻿namespace MyApp
{
    using System;

    using Coredian;
    using Coredian.Privacy;
    using UnityEngine;

    /// <summary>
    /// Examples of consent handling class.
    /// </summary>
    public class ConsentHandler : MonoBehaviour
    {
        private void Awake()
        {
            Core.GetService<IConsentService>().ConsentStatusChanged += OnConsentStatusChanged;
        }

        private void OnConsentStatusChanged(ConsentType consentType, ConsentStatus consentStatus)
        {
            if (consentType == ConsentType.Profiling)
            {
                if (consentStatus == ConsentStatus.Granted) StartProfiling();
                else StopProfiling();
            }
        }

        private void StopProfiling()
        {
        }

        private void StartProfiling()
        {
        }

        private void GetConsentStatus()
        {
            Core.GetService<IConsentService>().GetConsentStatus(ConsentType.Analytics);
        }

        private void SetConsentStatus(ConsentStatus consentStatus)
        {
            Core.GetService<IConsentService>().SetConsentStatus(ConsentType.Analytics, consentStatus);
        }

        private async void AskingForConsent()
        {
            // AskForConsentAsync must be awaited.
            var askForConsentResult = await Core.GetService<IConsentService>().AskForConsentAsync(ConsentType.PrivacyPolicy | ConsentType.Analytics);
 
            if (!string.IsNullOrEmpty(askForConsentResult.Error))
            {
                Log.Error($"Failed to show consent with error: '{askForConsentResult.Error}'");
                return;
            }
 
            foreach (var consentState in askForConsentResult.ConsentsStates)
            {
                if (consentState.ConsentType == ConsentType.PrivacyPolicy)
                {
                    if (consentState.ConsentStatus == ConsentStatus.Granted)
                    {
                        LoadGame();
                    }
                    else
                    {
                        // Did not get privacy policy consent - application cannot start.
                        ShowStartFailPopup();
                    }
                }
            }
        }

        private async void OpenConsentManagement()
        {
            // Await consent management view without storing the result.
            // This code assumes that classes that use consent are subscribed to ConsentStatusChanged event.
            _ = await Core.GetService<IConsentService>().OpenConsentManagementAsync();
        }

        private void LoadGame()
        {
            // Load game here.
        }

        private void ShowStartFailPopup()
        {
            // Show failure popup here.
        }
    }
}