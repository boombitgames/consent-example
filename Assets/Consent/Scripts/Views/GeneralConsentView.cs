﻿namespace MyApp.Consent
{
	using System;
	using System.Collections.Generic;
	using System.Threading.Tasks;

	using Coredian.Privacy;

	using UnityEditor;

	/// <summary>
	/// General consent view able to handle Analytics, Profiling, Age and Privacy Policy consents.
	/// </summary>
	public class GeneralConsentView : IConsentView
	{
		/// <inheritdoc />
		public ConsentType SupportedConsentTypes => ConsentType.Analytics |
		                                            ConsentType.Profiling |
		                                            ConsentType.Over16YearsOld |
		                                            ConsentType.PrivacyPolicy;

		private const string ConsentWindowResource = "GeneralConsentWindow";

		/// <inheritdoc />
		public async Task<ConsentViewResult> Show(IReadOnlyList<ConsentState> consentsStates)
		{
			// Load window from resources.
			var window = Core.UI.LoadAndRegisterWindow(ConsentWindowResource) as GeneralConsentWindow;
			
			// Wait for window to ask user for consent and read the results. This method displays the window
			// and waits for user response. When response is received, it returns user's response.
			var resultStates = await window.AskForConsent(consentsStates);
			
			// Unload the window (clean up of the resources).
			Core.UI.UnloadAndUnregisterWindow(window);
			
			// If statuses are null it means that window was canceled.
			if (resultStates == null) return new ConsentViewResult("Window canceled error");
			
			// Return ConsentViewResult with received statuses.
			return new ConsentViewResult(resultStates);
		}
	}

}