﻿using UnityEngine;

namespace MyApp.Consent
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Threading.Tasks;

	using Coredian.Privacy;
	using Coredian.UI;

	/// <summary>
	/// Window able to display UI of Analytics, Profiling, Age and Privacy Policy consents.
	/// </summary>
	public class GeneralConsentWindow : ModalWindow
	{
		/// <summary>
		/// Toggle of each consent.
		/// </summary>
		[SerializeField]
		public List<ConsentToggle> consentToggles;

		/// <summary>
		/// Completion source used for 
		/// </summary>
		private TaskCompletionSource<IReadOnlyList<ConsentState>> showCompletionSource;

		/// <summary>
		/// Stores user's response.
		/// </summary>
		private IReadOnlyList<ConsentState> userResponse;

		/// <summary>
		/// Called by OK button.
		/// </summary>
		public void OnOkButtonClicked()
		{
			// Set user's response based on state of toggles.
			userResponse = CollectResult();
			Hide();
		}

		/// <summary>
		/// Called by Cancel button.
		/// </summary>
		public void OnCancelButtonClicked()
		{
			// Set user's response to null.
			userResponse = null;
			Hide();
		}

		/// <summary>
		/// Ask for consents for specific statuses.
		/// </summary>
		/// <param name="consentStates">Consent statuses.</param>
		public async Task<IReadOnlyList<ConsentState>> AskForConsent(IReadOnlyList<ConsentState> consentStates)
		{
			// Set up state of toggles based on received statuses.
			foreach (var consentStatus in consentStates)
			{
				var toggle = consentToggles.First(t => t.consentType == consentStatus.ConsentType);

				toggle.toggle.isOn = consentStatus.ConsentStatus == ConsentStatus.Granted;
			}

			// Reset previous user's response.
			userResponse = null;

			// AskForConsent is an async method. Because it needs to wait for a reaction from user on UI window,
			// and UI does not use async, we use TaskCompletionSource to simulate async task.
			showCompletionSource = new TaskCompletionSource<IReadOnlyList<ConsentState>>();

			// Show window.
			Show();

			// Await for user reaction.
			await showCompletionSource.Task;

			return showCompletionSource.Task.Result;
		}

		/// <inheritdoc />
		protected override void OnHidden()
		{
			// When window is hidden, set completion source to user's response.
			showCompletionSource.SetResult(userResponse);
			base.OnHidden();
		}

		/// <summary>
		/// Collect states of toggles into list of consent states.
		/// </summary>
		private IReadOnlyList<ConsentState> CollectResult()
		{
			return consentToggles.Select(
									 ct => new ConsentState(
										 ct.consentType,
										 ct.toggle.isOn ? ConsentStatus.Granted : ConsentStatus.Denied))
								 .ToList();
		}
	}
}
