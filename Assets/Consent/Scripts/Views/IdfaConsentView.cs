﻿namespace MyApp.Consent
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Coredian;
    using Coredian.Privacy;
    using Coredian.Localization;
    using Coredian.UI.PredefinedWindows;

    /// <summary>
    /// Test consent view that asks for AdvertisingIdentifier consent.
    /// </summary>
    public class IdfaConsentView : IConsentView
    {
        /// <inheritdoc />
        public ConsentType SupportedConsentTypes => ConsentType.AdvertisingIdentifier;

        /// <inheritdoc />
        public async Task<ConsentViewResult> Show(IReadOnlyList<ConsentState> consentsStates)
        {
            // Get message dialog. This window is managed by UI module, so there's no need to load or unload it.
            var messageDialogWindow = Core.UI.GetWindow<IMessageDialogWindow>();

            // Create task completion source to simulate async call.
            var taskCompletionResult = new TaskCompletionSource<ConsentViewResult>();

            var status = consentsStates.First(cs => cs.ConsentType == ConsentType.AdvertisingIdentifier);
            // Validate that only one status was passed and it's AdvertisingIdentifier consent status.
            if (consentsStates.Count > 1 || status.ConsentType != ConsentType.AdvertisingIdentifier)
            {
                Log.Error(
                    "IdfaConsentView can only handle advertising identifier consent. Check your consent config node.");
                return new ConsentViewResult(
                    "IdfaConsentView can only handle advertising identifier consent. Check your consent config node.");
            }
            
            // Show the message dialog and set the result base on response in callback.
            messageDialogWindow.Show(
                new MessageDialogContent
                {
                    titleContent = new LocalizedText("IDFA Consent", false),
                    textContent = new LocalizedText(
                        $"Current idfa consent status is '{status}'. Do you allow use of IDFA by this app?",
                        false),
                    buttonsContent = new[] {new LocalizedText("Yes", false), new LocalizedText("No", false)},
                    buttonToPressWhenHiding = 1
                },
                button =>
                {
                    ConsentViewResult result;
                    if (button != 0 && button != 1)
                        result = new ConsentViewResult("Invalid response to idfa message dialog");
                    else
                    {
                        result = new ConsentViewResult(
                            new List<ConsentState>
                            {
                                new ConsentState(
                                    ConsentType.AdvertisingIdentifier,
                                    button == 0 ? ConsentStatus.Granted : ConsentStatus.Denied)
                            });
                    }

                    taskCompletionResult.SetResult(result);
                });

            // Await the task to complete.
            await taskCompletionResult.Task;
            return taskCompletionResult.Task.Result;
        }
    }
}