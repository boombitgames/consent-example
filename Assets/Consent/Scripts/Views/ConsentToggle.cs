﻿namespace MyApp.Consent
{
    using System;
    using Coredian.Privacy;
    using UnityEngine.UI;
    
    /// <summary>
    /// Consent toggle.
    /// </summary>
    [Serializable]
    public class ConsentToggle
    {
        /// <summary>
        /// Type of consent.
        /// </summary>
        public ConsentType consentType;

        /// <summary>
        /// The toggle.
        /// </summary>
        public Toggle toggle;
    }
}