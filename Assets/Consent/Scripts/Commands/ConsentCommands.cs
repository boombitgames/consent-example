﻿namespace MyApp.Consent
{
	using Coredian.Privacy;
	using Coredian.Events;

	/// <summary>
	/// Commands for event bindings.
	/// </summary>
	public class ConsentCommands
	{
		/// <summary>
		/// Ask for specific consent command.
		/// </summary>
		/// <param name="consentTypeEventParameter">Parameter containing consent type.</param>
		[EventMethod]
		public static void AskForConsent(ConsentTypeEventParameter consentTypeEventParameter)
		{
			_ = Core.GetService<IConsentService>().AskForConsentAsync(consentTypeEventParameter.consentType);
		}

		/// <summary>
		/// Ask for all consent command.
		/// </summary>
		[EventMethod]
		public static void AskForConsent()
		{
			_ = Core.GetService<IConsentService>().AskForConsentAsync(ConsentType.All);
		}
	}
}
