﻿namespace MyApp.Consent
{
	using Coredian.Privacy;
	using Coredian.Events;

	/// <summary>
	/// Event parameter allowing passing consent type to event method.
	/// </summary>
	public class ConsentTypeEventParameter : EventParameterComponent
	{
		/// <summary>
		/// Type of consent.
		/// </summary>
		public ConsentType consentType;
	}

}